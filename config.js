module.exports = {
    proyect_location: __dirname,
    //express
    port: process.env.port || 80,
    // jwt
    SECRET_TOKEN: 'Zr3gmOc6',
    // utilizada para login 
    key: 'C3t@',
    // arduino
    // COM_arduino: 'COM20',
    arduino: process.env.arduino || 0,  // el booleano se puso como string ya que la variable de entorno devuele strings :|
    COM_arduino: process.env.COM_arduino || 'COM20',
    COM_volumen: 'COM5',
    // frontales, laterales, traseras, gradas
    pines_arduino: [13,12,11,10,9,8,7,6],
    tiempo_cortinas: 16000,
    tiempoMitad_cortinas: 8000,
    rutaLog: 'http://betacerebro.ufm.edu/cerebro/public/api/logautomatizacion',
    default_estado_cortinas: process.env.default_estado_cortinas || 1,
}

