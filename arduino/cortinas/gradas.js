'use strict'

const arduino = require('../')
const config = require('../../config')
const LocalStorage = require('node-localstorage').LocalStorage
let localStorage = new LocalStorage(`${config.proyect_location}/storage/storage_cortinas_gradas`)

localStorage.setItem('estado', config.default_estado_cortinas) // estado inicial

function cortina(accion) {

    return new Promise((resolve, reject) => {

        try {

            switch (accion) {

                case 'subir':
                    if (localStorage.getItem('estado') == 0.5) {
                        console.log('cortinas - subiendo gradas desde mitad')
                        arduino.stepsUpON()
                        setTimeout(() => {
                            arduino.stepsUpOFF()
                            localStorage.setItem('estado', 1) // 1 = arriba                     
                            resolve()
                        }, config.tiempoMitad_cortinas)
                    } else if (localStorage.getItem('estado') == 0) {
                        console.log('cortinas - subiendo gradas')
                        arduino.stepsUpON()
                        setTimeout(() => {
                            arduino.stepsUpOFF()
                            localStorage.setItem('estado', 1) // 1 = arriba
                            resolve()
                        }, config.tiempo_cortinas)
                    } else if (localStorage.getItem('estado') == 1) {
                        resolve()
                    }
                    break

                case 'bajar':
                    if (localStorage.getItem('estado') == 0.5) {
                        console.log('cortinas - bajando gradas desde la mitad')
                        arduino.stepsDownON()
                        setTimeout(() => {
                            arduino.stepsDownOFF()
                            localStorage.setItem('estado', 0) // 0 = abajo                            
                            resolve()
                        }, config.tiempoMitad_cortinas)
                    } else if (localStorage.getItem('estado') == 1) {
                        console.log('cortinas - bajando gradas')
                        arduino.stepsDownON()
                        setTimeout(() => {
                            arduino.stepsDownOFF()
                            localStorage.setItem('estado', 0) // 0 = abajo
                            resolve()
                        }, config.tiempo_cortinas)
                    } else if (localStorage.getItem('estado') == 0) {
                        resolve()
                    }
                    break

                case 'mitad':
                    if (localStorage.getItem('estado') == 1) {
                        console.log('cortinas - bajando gradas hacia la mitad')
                        arduino.stepsDownON()
                        setTimeout(() => {
                            arduino.stepsDownOFF()
                            localStorage.setItem('estado', 0.5) // 0.5 = mitad                
                            resolve()
                        }, config.tiempoMitad_cortinas)
                    } else if (localStorage.getItem('estado') == 0) {
                        console.log('cortinas - subiendo gradas hacia la mitad')
                        arduino.stepsUpON()
                        setTimeout(() => {
                            arduino.stepsUpOFF()
                            localStorage.setItem('estado', 0.5) // 0.5 = mitad                
                            resolve()
                        }, config.tiempoMitad_cortinas)
                    } else if (localStorage.getItem('estado') == 0.5) {
                        resolve()
                    }
                    break

                default:
                    reject({
                        status: 400,
                        message: 'accion no disponible'
                    })
                    break

            }
        } catch (e) {
            reject({
                status: 500,
                message: 'error al procesas cortinas: gradas'
            })
        }
    })
}

module.exports = cortina